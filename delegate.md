---
layout: page
title: Delegate
permalink: /delegate/
featured-img: cubes.png
---

If you are looking to organize a cube competition in New England, you're in the right place! Please read this **entire** page before getting in touch with your local Delegate, as it is important that you understand exactly what organizing involves.

[Here](https://docs.google.com/spreadsheets/d/1-qjvGF3loVR0sr-AaDGZE2C7YgwcC1dP2wdPc3BT8fM/edit?usp=sharing) is a link to the competition organization spreadsheet. It will be discussed later on this page. Please go to this page, go to File > Make a Copy, and rename it to your competition.

## Are You Qualified?
If would like to organize, you should have a very good understanding of how competitions work. This does not mean you need to know everything, but you should have helped out significantly during at least a few competitions. If you have helped out significantly, you've probably interacted with a Delegate and they probably know who you are. You should also have a general idea of which events are popular, how events typically take, and how different staff roles help the competition run.

Regardless of prior experience, organizing is much easier on a team. We suggest organizing with at least one other person due to the time commitment required. We do have some local organizers who we may be able to connect with that have expressed interest in helping new organizers.

## Getting in Touch

Once you are ready to contact us, please send an email to newengland@cubingusa.org. This address goes to all local Delegates and some veteran organizers who are happy to help out. We do our best to respond promptly, but if you don't hear back within a week, please follow up to make sure we did not miss your message.

## Role of the Organizer and the Delegate
The organizer and Delegate have several roles, but these can vary by region throughout the world. Here we want to describe exactly what the Delegate will do, and what the organizer (you) will need to do.

### The Date
Competitions have to be about 3 weeks apart if they are within 100km of each other by WCA Regulations. Work with the Delegate to select a date that works for the Delegate, the organizers, and the venue. Keep in mind any major holidays or other events that could affect turnout.

### The Venue
The organizer is entirely responsible for choosing and booking the venue. The Delegate must approve the venue, so do not book without explicit Delegate approval. In general, a venue should have:
- enough space for everyone (there is usually one guest per competitor, so the room should be able to easily hold double the competitor limit)
- an internet connection, preferably WiFi
- a reasonable temperature
- a reasonable price (this will be discussed later)
- a PA system (nice to have, but not a requirement)
- a projector (also nice to have but not necessary)
- tables (competitor tables are absolutely required or must be rented, tables for the audience are also nice)
- lighting that is appropriate for solving that will be relatively consistent throughout the day

Schools and universities can often provide low-cost venues. Be sure to make note of required custodians or police officers, as this usually costs extra. Community centers or places of worship (like churches) are also good candidates.

### PA System, WiFi
It’s preferable to have a PA system, but let the Delegate know in advance if the venue doesn’t have one so other arrangements can be made. Similarly, it's possible to have a competition without WiFi, but it is difficult and requires planning with the Delegate.

### Finances
The organizer is responsible for creating a budget, but the Delegate will need to approve it. The organizer will usually need to create a Stripe account to collect the registration fees and link this to the WCA website. Please fill out the budget tab in the spreadsheet linked to at the top of this document when it's time to make a budget. Note that you should plan for registration income to cover printing costs, battery costs, and any other costs related to the competition while still leaving a safe margin to ensure positive net income. We generally prefer to use net income from a competition to purchase future equipment for the region.

### Events
Work with the delegate to select the events for the competition. In general, at most competitions we recommend holding a few events like 3x3x3, 2x2x2, and Pyraminx that are popular with a large number of competitors. The organizer should create a tentative schedule based on what seems realistic. It's good to add buffer time in case some events run longer than scheduled. The Delegate will help adjust the schedule to be more reasonable if needed.

### The Schedule
The organizer is responsible for creating the schedule, and the Delegate will approve it and make suggestions to try to accurately reflect realistic times. Please fill out the scheduling section of the spreadsheet at the top of this document when it's time to make a schedule. 

Competitions should have a new competitor tutorial at or around the beginning of the day before any popular events have started. They should also have some buffer time during the day in case the competition falls behind schedule. One way to do this is to make lunch longer than it needs to be so that it can also serve as a buffer if needed.

### Competitor Limits
Competitor limits are strictly enforced as per WCA Regulations. No competitors can be accepted when the limit is reached under any circumstances, so be cautious when accepting registrations. This means that organizers must carefully consider the competitor limit. There are typically twice the number of people at the competition as there are competitors (so 200 total people if the cap is 100). Most local competitions have caps of about 90-150. In areas with lower turnout, it is fine to go below 90, but only experienced organizers should attempt to organize competitions with over 150 competitors.

### Waitlists
It is a good idea for competitions to have a waitlist. This allows competitors who cannot make it to be replaced by others. Waitlist information must be posted on the website when the competition is announced Typically, waitlisted individuals are asked to pay, and then are refunded if they do not get a spot. This policy works best with a partial refund for competitors who drop out to encourage them to let the organizers know in advance. There should be a final date when the waitlist stops moving and refunds stop. Leave at least one week before the competition for this so there is time to print scorecards.

### The Website
The Delegate will create a template WCA website for the competition and send the organizer a link. This link will only be visible to organizers signed in with their WCA accounts. The organizers are responsible for filling in all relevant information, and then the Delegate will approve the site and send it to the WCA Competition Announcement Committee when it is ready to announce. It is good practice to leave at least a week between the announcement and the time when registration opens so competitors can learn more about the competition. Additionally, a week between the closing of registration and the competition itself gives organizers time to print scorecards and prepare groups.

### Competition Staff
We have a template of a Google Form we use to collect staff applications which the Delegate will set up. While Delegates can provide guidance, the organizer is responsible for providing lunch to the staff and properly coordinating with all staff members.

Staff typically have perks of free registration and provided lunch in exchange for helping out all day. Staff must pay to register and will be refunded after the competition to ensure that they do show up and help out. Staff applications are posted on the website to allow anyone to fill out a form and apply, but it is up to the Delegate’s discretion to vet these people and make sure qualified people will be running the competition.

### Lunch
The organizers can choose to provide lunch for the competitors and their families. This is particularly important if there are not many places to eat near the venue. Pizza is a cheap option, but large orders need to be arranged with the restaurant in advance. Often, the restaurant may offer special pricing for large orders.

### Sponsorship
Many competitions are sponsored by cube stores, who will provide prizes and sometimes even vend at the competition. Organizers can reach out to cube stores or other organizations if they are interested. This should be done before the competition is announced.

### CubingUSA Supported Competitions
CubingUSA has a [supported competition policy](https://cubingusa.org/supported).

The main benefits of a supported competition is their nonprofit status (which can result in venue discounts) and that finances are handled in a more official way. The Delegate can help the organizers determine if this fits the competition’s needs.

## After Announcing

After announcing the competition, it is critical for the organizers to respond to emails in a timely manner as competitors may reach out with questions. The Delegate may also have requests or questions.

### Accepting Registrations
Registration is done on the WCA website. When a competitor pays, they will need to be manually accepted by the organizer to be fully registered. This should be done promptly so the competitor can be sure they are registered.


### CubeComps
CubeComps is the main live results system for WCA competitions. Organizers should email admins@cubecomps.com after the competition is announced to set up a site. There is also some setup work involved in actually importing your registration file, and the organizer must also set up rounds and cutoffs within CubeComps.

### Scorecards and Certificates
The organizer is responsible for generating and printing scorecards as well as designing and printing the certificates. [Groupifier](https://jonatanklosko.github.io/groupifier-next/#/) is typically used for scorecard generation of first round cards and CubeComps is used to generate blank cards. You should post groups on the website when they have been generated.

Certificates are the responsibility of the organizers, the Delegate may be able to put you in touch with someone to help design them if needed.

### Pre-competition Email
A few days before the competition, it’s good to send out any important information by email. This includes groups, parking information, and any information that may have changed between the announcement and now.

### During the Competition
The Delegate will usually arrive early to help set up, but it is ideal when the venue can be set up the night before. The organizer is expected to arrive well before the start of the competition. The organizers are in charge of running the competition. The Delegate’s role is to ensure that WCA Regulations are being followed, not to run the event. That being said, Delegates have significant organizing experience and will serve as a resource to the organizer.

Some examples of organizer tasks include:
- making announcements when need
- ensuring scorecards are laid out for upcoming events
- managing staff to make sure all roles are filled as needed
- answering questions from or directing competitors and spectators as needed
- working with the venue's management to resolve any problems
- keeping the competitor area separated from the audience

## Wrapping up
Please get in touch with us if there's anything you feel should be added to this guide, or if you have any questions. We look forward to hearing from you soon!
