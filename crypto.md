---
layout: page
title: Cryptography
permalink: /crypto/
featured-img: crypto.jpg
repeat-background: true
---

I've created a few tools to help understand structures and algorithms used in cryptography.

If you have feedback, suggestions for future projects, or have found an issue, please [get in touch](/contact).

## Tools

[XOR Utility](xor) is a tool to help XOR a series of bits with another series of bits. This is useful for encoding or decoding a Vernam cipher, or in many other encryption schemes that use XOR in any way.

[Linear Feedback Shift Register](lfsr) constructs a linear feedback shift register (LFSR) based on user input and visualizes the process of generating the output. This is often used for pseudo-random number generation.

[DES Tools](des-tools) is a toolkit for understanding DES. It includes an expander, a permuter, and the S-boxes used in the DES encryption scheme.