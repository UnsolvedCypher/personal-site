---
layout: post
title: Cracking the NYT Mini Crossword's Hash
categories: [Programming]
# featured-img: hashmap
---

# Cracking the NYT Mini Crossword's Hash

Every day, the New York Times publishes a [Mini crossword](https://www.nytimes.com/crosswords/game/mini). It's quite a lot of fun and takes just a few minutes. The best part, is you can share your time with your friends.

When you complete the crossword, it gives you a little badge you can share, with a URL like this: [https://www.nytimes.com/badges/games/mini.png?d=2023-10-01&t=94&c=41150b60f3010f8c35a2d7c5c22d47b4](https://www.nytimes.com/badges/games/mini.png?d=2023-10-01&t=94&c=41150b60f3010f8c35a2d7c5c22d47b4) .

The structure of the URL is fairly straightforward, `d` is the date and `t` is how fast you solved it, in seconds. But interestingly, `c` appears to be some sort of hash, or checksum for security reasons. You can't just go and modify your time in the URL, you get a blank time in the image if you do this. So, if someone sends you a URL, you can know for sure that they actually completed the crossword in that time!

You might imagine that the hash is the result of signing the date and time with [some sort of secret key](https://en.wikipedia.org/wiki/Public-key_cryptography). But no. I tried a few of the most common hashing algorithms on the date and time. I also varied what was being hashed (the full URL, just the query parameters, etc). It turned out to be an MD5 hash!

To make a badge saying I solved this same puzzle in one second, all I have to do is run md5 on the query parameters of the puzzle, without the question mark:

```bash
$ echo -n "d=2023-10-01&t=1" | md5
e9c01d3c5d689e00c7a0d83ace22fe1a
```


Note the `-n` to omit the newline from `echo`'s output.

With that, we can construct the full URL: [https://www.nytimes.com/badges/games/mini.png?d=2023-10-01&t=1&c=e9c01d3c5d689e00c7a0d83ace22fe1a](https://www.nytimes.com/badges/games/mini.png?d=2023-10-01&t=1&c=e9c01d3c5d689e00c7a0d83ace22fe1a) . Pretty neat!
