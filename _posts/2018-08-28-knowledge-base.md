---
layout: post
title: Deploying a Personal Knowledge Base
categories: [Guides]
featured-img: knowledge-base
---

This guide will explain how to set up a simple, statically hosted personal knowledge base. We'll be using [MkDocs](https://www.mkdocs.org/) to create the knowledge base and [GitLab](https://www.mkdocs.org/) with [Netlify](https://www.netlify.com/) to host it (for free!). 

## Why

As you go through life, you accumulate a lot of knowledge. However, humans are not exactly known for being good at remembering things in the long term. Whether it's what you learned in class or how you got the formatting to work just right in Word, we all have knowledge that we would like to preserve and aren't very likely to remember. Tools like Evernote, NoteJoy, and even plain old paper often do work well for this. However, if you want these notes to be simpler to access and be very easy to share with others, this is a good solution. To see an example of a knowledge base, take a look at mine: [kb.matthewmcmillan.me](https://kb.matthewmcmillan.me)

## Installing Up MkDocs

MkdDocs is a pip package. Pip is Python's package manager. First, you should make sure your Python and pip installations are up to date. Obviously this will vary depending on your operating system.

Now, run ``pip install mkdocs`` to perform the installation.

## Setting Up MkDocs

You'll need to open a terminal window and navigate to the folder where you want to keep your knowledge base. Run the following:

    mkdocs new my-project
    cd my-project

And now you have a project! To take a look at it, run ``mkdocs serve`` and navigate to http://127.0.0.1:8000/ in your browser. You should see the default MkDocs page. 

Actually editing this knowledge base should be fairly intuitive, just change the Markdown files in the folder that was created. The names of the files control the names of the pages, and putting files in directories will create a hierarchy in the knowledge base. A tool like [Forestry](https://forestry.io/) could help out with editing more seamlessly, but I'll save that for another guide. Likewise, there's a ton of configuration you can do with MkDocs, but it's already very well documented [here](https://www.mkdocs.org/). I would highly encourage you to add a theme.

You'll also need to create a requirements.txt file in the main folder for Netlify. All you need in the file is `mkdocs>=1.0.1` (this was the latest version at the time of this writing, but that may have changed since). If you're using a theme, make sure you include it too. For example, `mkdocs-material>=2.7.3`.

## Setting up GitLab

You'll need a GitLab account, so if you don't have one, you should go ahead and do that. You should also have git set up on your computer. 

Once you're signed into GitLab, click "New Project" in the upper left. You can make it private if you want, but that's up to you. Name your project something like "knowledge-base" and click "Create Project". On the next page, copy your project's URL and run the following. Be sure to replace the URL given with the URL you just copied.

    git init
    git remote add origin https://gitlab.com/YourAccount/knowledge-base.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master

That's it! Now, every time you make a change locally, commit it to this repository. Or, you can always edit using GitLab's web interface.

## Setting up Netlify

Create a Netlify account if you don't already have one. Once you're signed in, click on "New Site from Git", select "GitLab", and choose your Knowledge Base repository. Under the build setup, enter `mkdocs build` as your build command and `site` as your build directory. Click "Deploy", and your site will go live! Visit the URL Netlify provides you to take a look.

## Further Ideas

There are a lot of things you can do to make your knowledge base even better. Firstly, you may want to pick a theme or customize the CSS. All of this is documented [here](https://www.mkdocs.org/). Your website name is also probably very weird. You might want to link it to your own domain name if you have one, or at the very least, use a nicer Netlify subdomain. Netlify documents the process [here](https://www.netlify.com/docs/custom-domains/). Also, as I mentioned before, you could use [Forestry](https://forestry.io/) to edit your knowledge base. That might be nice if you're not comfortable with Markdown, but you can always edit on GitLab's online interface if you want to make changes without pulling and pushing from your computer.