---
layout: page
title: About
permalink: /about/
---

<img class="profile-image" src="/assets/img/matthew-mcmillan.jpg" alt="Matthew McMillan" />
  
<br /><br />

## About Me

My name is Matthew McMillan. I'm currently an undergraduate student at [Worcester Polytechnic Institute](https://www.wpi.edu/) studying Computer Science. I'm particularly interested in developing interesting and useful applications. I'm currently working on a Django webapp that creates scorecards for Rubik's Cube competitions.

Outside of computer science, I am active in the World Cube Association, which sanctions Rubik's cube competitions. I am a WCA Delegate and have helped officiate over 15 competitions. As a result, some of my programming projects have been related to improving processes for cubers or for the World Cube Association.

I'm also particularly interested in language learning. I speak English, Russian, Spanish, and Esperanto and am currently learning Dutch.